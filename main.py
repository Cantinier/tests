from flask import Flask, request

app = Flask(__name__)


@app.route('/check_adult', methods=['POST'])
def check_adult():
    age = request.json["age"]
    answer = dict()

    if type(age) == int:
        if age >= 18:
            answer['is_adult'] = 1
            answer['message'] = "is_adult"
        else:
            answer['is_adult'] = 0
            answer['message'] = "is_not_adult"
    else:
        answer['is_adult'] = -1
        answer['message'] = "var age is not int type"
    return answer

app.run(host='localhost', port=8090)