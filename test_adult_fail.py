import pytest
from requests import post
import allure


@allure.feature('Проверка теста - падение')
@allure.story('Сервис определения совершеннолетия')
@pytest.mark.parametrize('value, exp_value', [(18, 1)])
def test_adult_fail_code(value, exp_value, get_service):
    data = dict()
    data['age'] = value
    with allure.step("Отправить данные в формате json"):
        answer = post(get_service.get_url(), json=data)
    with allure.step("Проверить, что сервис вернул код ответа 300"):
        assert answer.status_code == 300, f'Неверный код ответа. Ожидаем 300, вернулся {answer.status_code}'


@allure.feature('Проверка теста - падение')
@allure.story('Сервис определения совершеннолетия')
@pytest.mark.parametrize('value, exp_value', [(18, 10)])
def test_adult_fail_data(value, exp_value, get_service):
    data = dict()
    data['age'] = value
    with allure.step("Отправить данные в формате json"):
        answer = post(get_service.get_url(), json=data)
    with allure.step("Из ответа получить значение параметра is_adult"):
        answer_value = answer.json()["is_adult"]
    with allure.step("Проверить, что is_adult определяется корректно"):
        assert exp_value == answer_value, f'test_value: {value}, exp_value: {exp_value}, answer_value: {answer_value}'



