from requests import post
import allure


@allure.feature('Проверка обработки не числового значения')
@allure.story('Сервис определения совершеннолетия')
def test_adult_message_with_incorrect_datatype(get_service):
    data = dict()
    data['age'] = 'aweqwe'
    with allure.step("Отправить данные в формате json"):
        answer = post(get_service.get_url(), json=data)
    with allure.step("Проверить, что сервис вернул код ответа 200"):
        assert answer.status_code == 200, f'Неверный код ответа. Ожидаем 200, вернулся {answer.status_code}'
    with allure.step("Из ответа получить значение параметра message"):
        answer_value = answer.json()["message"]
    with allure.step("Проверить, что message содержит значение 'var age is not int type'"):
        assert answer_value == 'var age is not int type', \
            f'exp_value: var age is not int type, answer_value: {answer_value}'

