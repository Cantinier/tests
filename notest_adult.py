import pytest
from requests import post


@pytest.fixture(scope="session")
def get_service():
    from Site import Service
    driver = Service()
    return driver


@pytest.mark.parametrize("value, expected", [(18, 1), (19, 1), (17, 0)])
def test_adult_value_with_age(value, expected, get_service):
    json_data = dict()
    json_data['age'] = value
    answer = post(get_service.get_url(), json=json_data).json()
    result = answer["is_adult"]
    assert result == expected, f"test_value: {value}, expected_result: {expected}, answer_result {result}"


def test_adult_message_with_incorrect_data(get_service):
    json_data = dict()
    json_data['age'] = 'qweqwe'
    answer = post(get_service.get_url(), json=json_data)
    result = answer.json()['message']
    assert result == 'var age is not int type', 'Message not correct'
